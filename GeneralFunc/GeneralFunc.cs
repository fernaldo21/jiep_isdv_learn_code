namespace LearCore.GeneralFunc
{
    public class GeneralFuncClass
    { 
        private decimal pDecResult;
        public static int GetDataAdd(int sIntValue1, int sIntValue2){  
            var iIntResult = sIntValue1 + sIntValue2;
            return iIntResult;
        }

        public decimal GetDataDivide(int sIntValue1, int sIntValue2){  
            pDecResult= sIntValue1 / sIntValue2;
            return pDecResult;
        }
    }
}